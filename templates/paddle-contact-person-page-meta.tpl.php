<?php

/**
 * @file
 * Template for the Page view of the Contact person.
 */
?>
<?php if (!empty($address)) : ?>
  <h3 class="paddle-cp-heading"><?php print t('Location'); ?></h3>
  <div class="paddle-cp paddle-cp-address">
    <i class="fa fa-home valigntop"></i>
    <div class="paddle-cp-address-container">
      <?php print $address; ?>
    </div>
  </div>
<?php endif;
if (!empty($skype) || !empty($fax)  || !empty($website) || !empty($linkedin) || !empty($twitter) || !empty($yammer) || !empty($ou_level_1) || !empty($ou_level_2) || !empty($ou_level_3)): ?>
  <h3 class="paddle-cp-heading"><?php print t('Other information'); ?></h3>
  <?php if (!empty($fax)) : ?>
    <div class="paddle-cp paddle-cp-fax">
      <i class="fa fa-print valigntop"></i>
      <?php print $fax; ?>
    </div>
  <?php endif;
  if (!empty($website)) : ?>
    <div class="paddle-cp paddle-cp-website">
      <i class="fa fa-link valigntop"></i>
      <?php print $website; ?>
    </div>
  <?php endif;
  if (!empty($linkedin)) : ?>
    <div class="paddle-cp paddle-cp-linkedin">
      <i class="fa fa-linkedin valigntop"></i>
      <?php print $linkedin; ?>
    </div>
  <?php endif;
  if (!empty($twitter)) : ?>
    <div class="paddle-cp paddle-cp-twitter">
      <i class="fa fa-twitter valigntop"></i>
      <?php print $twitter; ?>
    </div>
  <?php endif;
  if (!empty($yammer)) : ?>
    <div class="paddle-cp paddle-cp-yammer">
      <i class="icon-yammer valigntop"></i>
      <?php print $yammer; ?>
    </div>
  <?php endif;
  if (!empty($skype)) : ?>
    <div class="paddle-cp paddle-cp-skype">
      <i class="fa fa-skype valigntop"></i>
      <?php print $skype; ?>
    </div>
  <?php endif;
  if (!empty($manager)) : ?>
    <div class="paddle-cp paddle-cp-manager">
      <span class="label"><?php print t('Managed by'); ?></span>
      <?php print $manager; ?>
    </div>
  <?php endif;
  if (!empty($ou_level_1)) : ?>
    <div class="paddle-cp paddle-cp-ou-levels level-1">
     <?php print $ou_level_1; ?>
    </div>
  <?php endif;
  if (!empty($ou_level_2)) : ?>
    <div class="paddle-cp paddle-cp-ou-levels level-1">
      <?php print $ou_level_2; ?>
    </div>
  <?php endif;
  if (!empty($ou_level_3)) : ?>
    <div class="paddle-cp paddle-cp-ou-levels level-1">
      <?php print $ou_level_3; ?>
    </div>
  <?php endif;
endif;
?>
